/*
 * All rights reserved. (c) FireAdmin.de
 */
package de.sort.sort;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jens
 */
public class TournamentSort2 implements SortAlgorithm {

	@Override
	public List<Integer> sort(List<Integer> source) {
		ArrayList<Integer> target = new ArrayList<>();

		ArrayList<List<Integer>> working = new ArrayList<>();
		working.add(source);

		fill(source, working);

		Integer greatest = getGreatest(working);
		while (greatest != null) {
			target.add(greatest);
			greatest = getGreatest(working);
		}
		return target;
	}

	private void fill(List<Integer> source, List<List<Integer>> working) {
		if (source.size() > 1) {
			for (int i = 0; i < source.size(); i = i + 2) {
				ArrayList<Integer> cur = new ArrayList<>();
				Integer a = source.get(i);
				Integer b = (i + 1 < source.size() ? source.get(i + 1) : null);
				if (Integer.compare(a, b) == 1) {
					cur.add(a);
				} else {
					cur.add(b);
				}
				working.add(cur);
				fill(cur, working);
			}
		}
	}

	private Integer getGreatest(List<List<Integer>> working) {
		Integer greatest = working.get(working.size() - 1).get(0);
		resort(working);
		return greatest;
	}

	private void resort(List<List<Integer>> working) {
		for (int i = 0; i < working.size(); i++) {

		}
	}
}
