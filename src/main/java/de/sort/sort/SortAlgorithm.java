/*
 * All rights reserved. (c) FireAdmin.de
 */
package de.sort.sort;

import java.util.List;

/**
 *
 * @author jens
 */
public interface SortAlgorithm {

	abstract List<Integer> sort(List<Integer> source);
}
