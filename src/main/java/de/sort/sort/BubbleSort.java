/*
 * All rights reserved. (c) FireAdmin.de
 */
package de.sort.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author jens
 */
public class BubbleSort implements SortAlgorithm {

	@Override
	public List<Integer> sort(List<Integer> source) {
		ArrayList<Integer> target = new ArrayList<>();
		target.addAll(source);

		boolean sortChanged = false;
		int targetSize = target.size() - 1;
		do {
			sortChanged = false;
			for (int i = 0; i < targetSize; i++) {
				if (target.get(i) < target.get(i + 1)) {
					Collections.swap(target, i, i + 1);
					sortChanged = true;
				}
			}
		} while (sortChanged);

		return target;
	}

}
