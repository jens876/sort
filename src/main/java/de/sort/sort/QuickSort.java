/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.sort.sort;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JHI
 */
public class QuickSort implements SortAlgorithm {

	@Override
	public List<Integer> sort(List<Integer> source) {
		ArrayList<Integer> target = new ArrayList(source);
		return quickSort(target);
	}

	private List<Integer> quickSort(List<Integer> source) {
		if (source.size() <= 1) {
			return source;
		}

		int averageIndex = source.size() - 1;

		Integer averageValue = source.get(averageIndex);

		for (int i = 0; i < averageIndex; i++) {
			Integer currentValue = source.get(i);
			if (currentValue < averageValue) {
				source.add(currentValue);
				source.remove(i);
				averageIndex--;
				i--;
			}
		}
		if (source.size() >= 3) {
			quickSort(source.subList(0, averageIndex));
			if (averageIndex < source.size() - 1) {
				quickSort(source.subList(averageIndex, source.size()));
			}
		}
		return source;
	}
}
