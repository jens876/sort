package de.sort.sort;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jens
 */
public class TournamentSort implements SortAlgorithm {

	@Override
	public List<Integer> sort(List<Integer> source) {
		ArrayList<Integer> target = new ArrayList<>();

		ArrayList<Element> working = new ArrayList<>();
		for (Integer val : source) {
			working.add(new Element(val));
		}

		Element top = createElements(working);

		Integer val = top.getGreaterAndDelete();
		while (val != null) {
			target.add(val);
			val = top.getGreaterAndDelete();
		}

		return target;
	}

	Element createElements(ArrayList<Element> elements) {
		Element ret = null;
		if (elements.size() > 1) {
			ArrayList<Element> working = new ArrayList<>();

			for (int i = 0; i < elements.size(); i = i + 2) {
				Element first = elements.get(i);
				Element second = i + 1 < elements.size() ? elements.get(i + 1) : null;
				Element parent = new Element(first, second);
				working.add(parent);
			}

			ret = createElements(working);
		} else if (elements.size() == 1) {
			ret = elements.get(0);
		}
		return ret;
	}

	private class Element {

		public Element(Integer value) {
			this.myValue = value;
		}

		public Element(Element first, Element second) {
			this.first = first;
			this.second = second;
		}
		private Element first = null;
		private Element second = null;

		Integer myValue = null;

		public Integer getGreater() {
			Integer greaterValue = null;
			if (myValue != null) {
				greaterValue = myValue;
			} else {
				Element greaterElement = getGreaterElement();
				if (null != greaterElement) {
					greaterValue = greaterElement.getGreater();
					myValue = greaterValue;
				}
			}
			return greaterValue;
		}

		private boolean isNullOrEmpty(Element element) {
			return element == null || element.getGreater() == null;
		}

		private Element getGreaterElement() {
			Element greaterElement = null;
			if (!isNullOrEmpty(first) && !isNullOrEmpty(second)) {
				if (first.getGreater() > second.getGreater()) {
					greaterElement = first;
				} else {
					greaterElement = second;
				}
			} else if (isNullOrEmpty(first) && !isNullOrEmpty(second)) {
				greaterElement = second;
			} else if (!isNullOrEmpty(first) && isNullOrEmpty(second)) {
				greaterElement = first;
			}
			return greaterElement;
		}

		private void deleteGreater() {
			myValue = null;
			Element greaterElement = getGreaterElement();
			if (null != greaterElement) {
				greaterElement.deleteGreater();
			}
		}

		private Integer getGreaterAndDelete() {
			Integer greaterValue = getGreater();
			if (null != greaterValue) {
				deleteGreater();
			}
			return greaterValue;
		}
	}
}
