package de.sort.sort;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

	public static void main(String[] args) {
		ArrayList<Integer> arr = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 1000 + 1);
			arr.add(randomNum);
		}

		checkAlgorithm(new TournamentSort(), new ArrayList<>(arr));
		checkAlgorithm(new BubbleSort(), new ArrayList<>(arr));
		checkAlgorithm(new QuickSort(), new ArrayList<>(arr));

		checkAlgorithm(new TournamentSort(), StaticSortData.createUnsortedList());
		checkAlgorithm(new BubbleSort(), StaticSortData.createUnsortedList());
		checkAlgorithm(new QuickSort(), StaticSortData.createUnsortedList());
	}

	static void checkAlgorithm(SortAlgorithm sortAlgorithm, List<Integer> toSort) {
		LocalTime beginTournament = LocalTime.now();
		System.out.println("Begin " + sortAlgorithm.getClass().toString() + ": " + beginTournament.toString());
		List<Integer> sorted = sortAlgorithm.sort(toSort);
		LocalTime endTournament = LocalTime.now();
		System.out.println("End " + sortAlgorithm.getClass().toString() + ": " + endTournament.toString());
		long tournamenDuration = ChronoUnit.MILLIS.between(beginTournament, endTournament);
		System.out.println(sortAlgorithm.getClass().toString() + " duration: " + tournamenDuration);
		System.out.println("Sorted by " + sortAlgorithm.getClass().toString() + ": " + sorted.toString());
	}
}
