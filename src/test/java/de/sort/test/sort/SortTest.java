package de.sort.test.sort;

import de.sort.sort.SortAlgorithm;
import de.sort.sort.StaticSortData;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.Assert;
import org.junit.Before;

public class SortTest<T extends SortAlgorithm> {

	private List<Integer> m_arr = null;
	private LocalTime begin = null;
	private LocalTime end = null;
	private T sortAlgorithm = null;

	public SortTest(T sortAlgorithm) {
		this.sortAlgorithm = sortAlgorithm;
	}

	@Before
	public void setUp() {
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 1000 + 1);
			list.add(randomNum);
		}
		m_arr = Collections.unmodifiableList(list);
	}

	public void begin(String testName) {
		begin = LocalTime.now();
		System.out.println(sortAlgorithm.getClass().getSimpleName() + " begin " + testName + ": " + begin.toString());
	}

	public void end(String testName) {
		end = LocalTime.now();
		System.out.println(sortAlgorithm.getClass().getSimpleName() + " end " + testName + ": " + end.toString());
		long tournamenDuration = ChronoUnit.MILLIS.between(begin, end);
		System.out.println(sortAlgorithm.getClass().getSimpleName() + " duration of " + testName + ": " + tournamenDuration);
	}

	public void sortRandomArray() {
		begin("random list");
		List<Integer> arrRandom = sortAlgorithm.sort(m_arr);
		end("random list");
		Assert.assertEquals(m_arr.size(), arrRandom.size());
		Integer last = null;
		for (Integer number : arrRandom) {
			if (null == last) {
				last = number;
				continue;
			}
			Assert.assertTrue(last >= number);
			last = number;
		}
	}

	public void sortStaticArray() {
		begin("static list");
		List<Integer> arrStatic = sortAlgorithm.sort(StaticSortData.UNSORTED_LIST);
		end("static list");
		Assert.assertEquals(StaticSortData.SORTED_LIST.size(), arrStatic.size());
		Assert.assertArrayEquals(StaticSortData.SORTED_LIST.toArray(), arrStatic.toArray());
	}
}
