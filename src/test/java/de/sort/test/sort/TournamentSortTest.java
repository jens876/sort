package de.sort.test.sort;

import de.sort.sort.TournamentSort;
import org.junit.Test;

public class TournamentSortTest extends SortTest<TournamentSort> {

	public TournamentSortTest() {
		super(new TournamentSort());
	}

	@Test
	@Override
	public void sortStaticArray() {
		super.sortStaticArray();
	}

	@Test
	@Override
	public void sortRandomArray() {
		super.sortRandomArray();
	}
}
