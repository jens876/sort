package de.sort.test.sort;

import de.sort.sort.QuickSort;
import org.junit.Test;

public class QuickSortTest extends SortTest<QuickSort> {

	public QuickSortTest() {
		super(new QuickSort());
	}

	@Test
	@Override
	public void sortStaticArray() {
		super.sortStaticArray();
	}

	@Test
	@Override
	public void sortRandomArray() {
		super.sortRandomArray();
	}
}
