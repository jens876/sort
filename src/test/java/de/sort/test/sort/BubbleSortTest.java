package de.sort.test.sort;

import de.sort.sort.BubbleSort;
import org.junit.Test;

public class BubbleSortTest extends SortTest<BubbleSort> {

	public BubbleSortTest() {
		super(new BubbleSort());
	}

	@Test
	@Override
	public void sortStaticArray() {
		super.sortStaticArray();
	}

	@Test
	@Override
	public void sortRandomArray() {
		super.sortRandomArray();
	}
}
